This is a modified version of the SceneKitDocumentViewer example that Apple distributed at WWDC 2012. This code may or may not be any more useful to you than the original Apple code. I made the changes because Apple's sample code always makes me want to fuss around with it until it looks like my code.

If you'd like to download this project, remember that Scene Kit requires Mountain Lion.

Here are the main changes I made:

* Changed the name of the project to SimpleSceneViewer.
* Changed the code to use my personal coding and commenting style (indenting with tabs, braces on their own line, messages instead of dot notation, and a bunch of other personal preferences).
* Renamed MyView to MySceneView and gave it a delegate with a method for handling errors.
* Added a second sample DAE file.
* UI changes:
	* Changed the xib to use Auto Layout.
	* Moved help text to the top.
	* Added a color well for setting the background color.
	* Added a checkbox to turn animation on and off.
	* Added pushbuttons to load the two sample scenes.

