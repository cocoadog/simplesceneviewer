//
//  MySceneView.m
//  SimpleSceneViewer
//
//  This is modified code from Apple's SceneKitDocumentViewer example.
//

#import "MySceneView.h"
#import "MySceneViewDelegate.h"

@implementation MySceneView
{
	SCNMaterial *selectedMaterial;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		[self _commonInit];
	}
	
	return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
	self = [super initWithFrame:frameRect];
	if (self)
	{
		[self _commonInit];
	}
	
	return self;
}

- (BOOL)loadSceneAtURL:(NSURL *)url
{
	NSMutableDictionary *sceneOptions = [NSMutableDictionary dictionary];

	// Clear selection.
	selectedMaterial = nil;

	// Create normals if absent.
	[sceneOptions setValue:[NSNumber numberWithBool:YES]
					forKey:SCNSceneSourceCreateNormalsIfAbsentKey];

	// Optimize the rendering by flattening the scene graph when possible.
	// Note that this prevents you from animating objects independently.
	// [agl] Nice to know this option exists, but the rendering seems smooth enough
	// on my (fast) machine without it.
	//[sceneOptions setValue:[NSNumber numberWithBool:YES]
	//				forKey:SCNSceneSourceFlattenSceneKey];

	// Load the scene.
	NSError *error;
	SCNScene *scene = [SCNScene sceneWithURL:url options:sceneOptions error:&error];

	if (scene == nil)
	{
		[self _tellDelegateAboutError:error];
		return NO;
	}

	// Put the scene into the view.
	[self setScene:scene];
	return YES;
}

- (BOOL)loadSceneNamed:(NSString *)sceneName
{
	if (sceneName == nil)
	{
		[self _tellDelegateAboutErrorDescription:@"sceneName must not be nil."];
		return NO;
	}

	NSURL *sceneURL = [[NSBundle mainBundle] URLForResource:sceneName withExtension:@"dae"];

	if (sceneURL == nil)
	{
		NSString *errorDescription = [NSString stringWithFormat:@"There is no .dae file named '%@' in the application bundle.",
									  sceneName];
		[self _tellDelegateAboutErrorDescription:errorDescription];
		return NO;
	}

	return [self loadSceneAtURL:sceneURL];
}

#pragma mark - NSResponder methods

- (void)mouseDown:(NSEvent *)event
{
	NSPoint mouseLocation = [self convertPoint:[event locationInWindow] fromView:nil];
	NSArray *hits = [self hitTest:mouseLocation options:nil];

	if ([hits count] > 0)
	{
		SCNHitTestResult *hit = [hits objectAtIndex:0]; // nearest object hit

		[self _selectNode:hit.node geometryElementIndex:hit.geometryIndex];
	}
	else
	{
		[self _selectNode:nil geometryElementIndex:NSNotFound];
	}

	[super mouseDown:event];
}

#pragma mark - NSDraggingDestination methods

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
	return NSDragOperationCopy;
}

- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender
{
	return NSDragOperationCopy;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
	NSPasteboard *pasteboard = [sender draggingPasteboard];

	if (![[pasteboard types] containsObject:NSFilenamesPboardType])
	{
		[self _tellDelegateAboutErrorDescription:@"There's no Filenames data type in the dragging pasteboard."];
		return NO;
	}

	NSData *data = [pasteboard dataForType:NSFilenamesPboardType];

	if (data == nil)
	{
		[self _tellDelegateAboutErrorDescription:@"There's no Filenames data in the dragging pasteboard."];
		return NO;
	}

	NSString *errorDescription;
	NSArray *filenames = [NSPropertyListSerialization propertyListFromData:data
														  mutabilityOption:kCFPropertyListImmutable
																	format:nil
														  errorDescription:&errorDescription];
	if (filenames == nil)
	{
		[self _tellDelegateAboutErrorDescription:@"The dragged data does not contain a serialized property list."];
		return NO;
	}

	if (![filenames isKindOfClass:[NSArray class]])
	{
		[self _tellDelegateAboutErrorDescription:@"The property list in the dragged data is not an array."];
		return NO;
	}

	if ([filenames count] == 0)
	{
		[self _tellDelegateAboutErrorDescription:@"The dragged data contains an empty array."];
		return NO;
	}

	NSURL *fileURL = [NSURL fileURLWithPath:[filenames objectAtIndex:0] isDirectory:NO];

	if (fileURL == nil)
	{
		[self _tellDelegateAboutErrorDescription:@"The dragged data contains an invalid file path."];
		return NO;
	}

	return [self loadSceneAtURL:fileURL];
}

#pragma mark - Private methods

- (void)_commonInit
{
	[self registerForDraggedTypes:[NSArray arrayWithObject:NSFilenamesPboardType]];
}

- (void)_selectNode:(SCNNode *)node geometryElementIndex:(NSUInteger)index
{
	// Unhighlight previous selection.
	[[selectedMaterial emission] removeAllAnimations];

	// Clear selection.
	selectedMaterial = nil;

	// If there's no new selection, we're all done.
	if (node == nil)
	{
		return;
	}

	// Convert geometry element index to material index.
	NSArray *materials = [[node geometry] materials];
	index = index % [materials count];

	// Make the material unique (i.e. unshared).
	SCNMaterial *unsharedMaterial = [materials[index] copy];
	[[node geometry] replaceMaterialAtIndex:index withMaterial:unsharedMaterial];

	// Select it.
	selectedMaterial = unsharedMaterial;

	// Animate it.
	CABasicAnimation *highlightAnimation = [CABasicAnimation animationWithKeyPath:@"contents"];

	[highlightAnimation setToValue:[NSColor blueColor]];
	[highlightAnimation setFromValue:[NSColor blackColor]];
	[highlightAnimation setRepeatCount:MAXFLOAT];
	[highlightAnimation setAutoreverses:YES];
	[highlightAnimation setDuration:0.5];
	[highlightAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];

	[[selectedMaterial emission] addAnimation:highlightAnimation forKey:@"highlight"];
}

- (void)_tellDelegateAboutError:(NSError *)error
{
	if ([(id)[self myDelegate] respondsToSelector:@selector(mySceneView:didGetError:)])
	{
		[[self myDelegate] mySceneView:self didGetError:error];
	}
}

- (void)_tellDelegateAboutErrorDescription:(NSString *)errorDescription
{
	NSError *error = [NSError errorWithDomain:NSCocoaErrorDomain
										 code:0
									 userInfo:@{ NSLocalizedDescriptionKey : errorDescription }];

	[self _tellDelegateAboutError:error];
}

@end
