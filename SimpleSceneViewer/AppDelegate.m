//
//  AppDelegate.m
//  SimpleSceneViewer
//
//  This is modified code from Apple's SceneKitDocumentViewer example.
//

#import "AppDelegate.h"
#import "MySceneView.h"

@implementation AppDelegate

- (void)awakeFromNib
{
	[[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
	
	// Load an example scene.
	[self showWarrior:nil];
}

#pragma mark - Action methods

- (IBAction)showWarrior:(id)sender
{
	[[self sceneView] loadSceneNamed:@"scene"];
}

// The dice.dae file comes from Turbo Squid.
- (IBAction)showDice:(id)sender
{
	[[self sceneView] loadSceneNamed:@"dice"];
}

#pragma mark - MySceneViewDelegate methods

- (void)mySceneView:(MySceneView *)sceneView didGetError:(NSError *)error
{
	NSString *errorDescription = [error localizedDescription];
	NSString *errorReason = [error localizedFailureReason];

	if ([errorDescription length] == 0)
	{
		errorDescription = @"Not specified.";
	}

	if ([errorReason length] == 0)
	{
		errorReason = @"Not specified.";
	}

	NSString *errorMessage = [NSString stringWithFormat:@"[ERROR] Error description: %@ Error reason: %@",
							  errorDescription, errorReason];
	NSLog(@"%@", errorMessage);

	[[NSAlert alertWithError:error] beginSheetModalForWindow:[self window]
											   modalDelegate:nil
											  didEndSelector:nil
												 contextInfo:NULL];
}

@end
