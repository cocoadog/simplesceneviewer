//
//  MySceneView.h
//  SimpleSceneViewer
//
//  This is modified code from Apple's SceneKitDocumentViewer example.
//

#import <SceneKit/SceneKit.h>

@protocol MySceneViewDelegate;

/*!
 * Accepts drag-and-drop. If you click on an element, highlights the element.
 */
@interface MySceneView : SCNView

@property (weak) IBOutlet id <MySceneViewDelegate> myDelegate;

- (BOOL)loadSceneAtURL:(NSURL *)url;

/*! Looks for .dae resource in the application bundle. */
- (BOOL)loadSceneNamed:(NSString *)sceneName;

@end
