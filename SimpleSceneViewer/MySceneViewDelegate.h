//
//  MySceneViewDelegate.h
//  SimpleSceneViewer
//
//  Created by Andy Lee on 8/1/12.
//  Copyright (c) 2012 Andy Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MySceneView;

@protocol MySceneViewDelegate

@optional

- (void)mySceneView:(MySceneView *)sceneView didGetError:(NSError *)error;

@end
