//
//  AppDelegate.h
//  SimpleSceneViewer
//
//  This is modified code from Apple's SceneKitDocumentViewer example.
//

#import <Cocoa/Cocoa.h>
#import "MySceneViewDelegate.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, MySceneViewDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet MySceneView *sceneView;

- (IBAction)showWarrior:(id)sender;
- (IBAction)showDice:(id)sender;

@end
